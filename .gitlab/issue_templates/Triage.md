[Triage process](https://about.gitlab.com/handbook/engineering/dev-backend/distribution/triage.html)

Links:

- [Issues to triage](https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=0&milestone_title=No+Milestone&scope=all&sort=created_date&state=opened)

- Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=0&label_name%5B%5D=awaiting+feedback&milestone_title=No+Milestone&page=3&scope=all&sort=updated_desc&state=opened) (start from last page)

- ~Triage [issue history](https://gitlab.com/gitlab-org/distribution/team-tasks/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=Triage)

/label ~Triage
